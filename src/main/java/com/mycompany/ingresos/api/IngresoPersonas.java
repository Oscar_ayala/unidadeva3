/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.ingresos.api;

import Controller.IngresoController;
import com.mycompany.ingresos.dao.IngresosJpaController;
import com.mycompany.ingresos.dao.exceptions.NonexistentEntityException;
import com.mycompany.ingresos.entity.Ingresos;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.PathParam;

/**
 *
 * @author Fabian
 */
@Path("personas")
public class IngresoPersonas {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarIngresos() {

        IngresosJpaController dao = new IngresosJpaController();

        List<Ingresos> lista = dao.findIngresosEntities();

        return Response.ok(200).entity(lista).build();

    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response agregar(Ingresos ingresos) {

        try {
            IngresosJpaController dao = new IngresosJpaController();
            dao.create(ingresos);
        } catch (Exception ex) {
            Logger.getLogger(IngresoPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(ingresos).build();

    }

    @DELETE
    @Path("/{iddelete}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminar(@PathParam("iddelete") String iddelete) {
        try {
            IngresosJpaController dao = new IngresosJpaController();
            dao.destroy(iddelete);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(IngresoPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok("Cliente eliminado").build();

    }
    
    @PUT
 public Response subir(Ingresos ingresos){
        try {
            IngresosJpaController dao=new IngresosJpaController();
            dao.edit(ingresos);
        } catch (Exception ex) {
            Logger.getLogger(IngresoPersonas.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.ok(200).entity(ingresos).build();
 }
}

package com.mycompany.ingresos.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 *
 * @author 
 */
@Path("api")
public class Appconfig {
    
    @GET
    public Response ping(){
        return Response
                .ok("ping")
                .build();
    }
}
